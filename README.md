# Flash-Chat

## Function:
Send and receive messages real-time from Firebase

## Detail:

* Store data in the cloud using Firebase Firestore
* Query and sort the Firebase database
* Use Firebase for user authentication, registration and login
* Work with UITableViews and how to set their data sources and delegates
* Create custom views using .xib files to modify native design components

